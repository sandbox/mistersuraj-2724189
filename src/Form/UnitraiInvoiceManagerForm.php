<?php
  
/**
 * @file
 * Contains \Drupal\unitrai_invoice_manager\Form\UnitraiInvoiceManagerForm.
 */
  
namespace Drupal\unitrai_invoice_manager\Form;
  
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
 
class UnitraiInvoiceManagerForm extends FormBase {
    
  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'unitrai_invoice_manager_form';
  }
    
  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('unitrai_invoice_manager.settings');
    $form['email'] = [
      '#type' => 'email',
      '#title' => $this->t('Your .com email address.'),
      '#default_value' => $config->get('email_address'),
    ];
    $form['show'] = [
      '#type' => 'submit',
      '#value' => $this->t('Submit'),
    ];
    return $form;
  }
    
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) { 
    if (strpos($form_state->getValue('email'), '.com') === FALSE) {
      $form_state->setErrorByName('email', $this->t('This is not a .com email address.'));
    }
  }
    
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    drupal_set_message($this->t('Your email address is @email', ['@email' => $form_state->getValue('email')]));
  }
    
}