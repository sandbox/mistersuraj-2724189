<?php
  
/**
 * @file
 * Contains \Drupal\unitrai_invoice_manager\Form\UnitraiInvoiceManagerConfigForm.
 */
  
namespace Drupal\unitrai_invoice_manager\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormStateInterface;
 
class UnitraiInvoiceManagerConfigForm extends ConfigFormBase {
 
   /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;
    
/**
* Constructs a \Drupal\unitrai_invoice_manager\Form\UnitraiInvoiceManagerConfigForm object.
*
* @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
*   The factory for configuration objects.
* @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
*   The module handler.
*/
  public function __construct(ConfigFactoryInterface $config_factory) {
        parent::__construct($config_factory);
       // $this->moduleHandler = $module_handler_interface;
  }
    
  /**
   * {@inheritdoc}.
   */
  public function getFormId() {
    return 'unitrai_invoice_manager_config_form';
  }
    
  /**
   * {@inheritdoc}.
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
      $form = parent::buildForm($form, $form_state);
      $config = $this->config('unitrai_invoice_manager.settings');
     
      $form['company'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Company'),
          '#default_value' => $config->get('company'),
      ];
      
      
      $form['logo'] = [
          '#type' => 'file',
          '#title' => $this->t('Logo')
      ];
      
      $form['address_1'] = [
          '#type' => 'textarea',
          '#title' => $this->t('Address'),
          '#rows' => 2,
          '#default_value' => $config->get('address.address_1').$config->get("logo"),
          '#style' => 'width:100px;'
      ];
      
      $form['city'] = [
          '#type' => 'textfield',
          '#title' => $this->t('City'),
          '#default_value' => $config->get('address.city'),
      ];
      
      $form['state'] = [
          '#type' => 'textfield',
          '#title' => $this->t('State'),
          '#default_value' => $config->get('address.state'),
      ];
      
      $form['zip'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Zip'),
          '#default_value' => $config->get('address.zip'),
      ];
      
      $form['country'] = [
          '#type' => 'textfield',
          '#title' => $this->t('Country'),
          '#default_value' => $config->get('address.country'),
      ];
      
     
    
    return $form;
  }
    
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) { 
      
      parent::validateForm($form, $form_state);
        
       // if ($this->moduleHandler->moduleExists("file")) {
                
                // Handle file uploads.
                $validators = array('file_validate_is_image' => array());
                
                // Check for a new uploaded logo.
                $file = file_save_upload('logo', $validators, FALSE, 0);
                if (isset($file)) {
                    // File upload was attempted.
                    if ($file) {
                      // Put the temporary file in form_values so we can save it on submit.
                      $form_state->setValue('logo', $file);
                    }
                    else {
                      // File upload failed.
                      $form_state->setErrorByName('logo', $this->t('The logo could not be uploaded.'));
                    }
                }

                
       //}
     
  }
  
  /**
   * Validates the path of the display.
   *
   * @param string $path
   *   The path to validate.
   *
   * @return array
   *   A list of error strings.
   */
  protected function validatePath($path) {
    $errors = array();
    if (strpos($path, '%') === 0) {
      $errors[] = $this->t('"%" may not be used for the first segment of a path.');
    }

    $parsed_url = UrlHelper::parse($path);
    if (empty($parsed_url['path'])) {
      $errors[] = $this->t('Path is empty.');
    }

    if (!empty($parsed_url['query'])) {
      $errors[] = $this->t('No query allowed.');
    }

    if (!parse_url('internal:/' . $path)) {
      $errors[] = $this->t('Invalid path. Valid characters are alphanumerics as well as "-", ".", "_" and "~".');
    }

    $path_sections = explode('/', $path);
    // Symfony routing does not allow to use numeric placeholders.
    // @see \Symfony\Component\Routing\RouteCompiler
    $numeric_placeholders = array_filter($path_sections, function ($section) {
      return (preg_match('/^%(.*)/', $section, $matches)
        && is_numeric($matches[1]));
    });
    if (!empty($numeric_placeholders)) {
      $errors[] = $this->t("Numeric placeholders may not be used. Please use plain placeholders (%).");
    }
    return $errors;
  }
    
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
        $config = $this->config('unitrai_invoice_manager.settings');
        $config->set('company', $form_state->getValue('company'));
        $config->set('logo', $form_state->getValue('logo'));
        $config->set('address.address_1', $form_state->getValue('address_1'));
        $config->set('address.city', $form_state->getValue('city'));
        $config->set('address.state', $form_state->getValue('state'));
        $config->set('address.zip', $form_state->getValue('zip'));
        $config->set('address.country', $form_state->getValue('country'));
        $config->save();
        return parent::submitForm($form, $form_state);
  }
  
  
   /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['unitrai_invoice_manager.settings'];
  }
    
}