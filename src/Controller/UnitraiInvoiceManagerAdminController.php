<?php

/**
@file
Contains \Drupal\unitrai_invoice_manager\Controller\UnitraiInvoiceManagerAdminController.
 */

namespace Drupal\unitrai_invoice_manager\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Description of UnitraiInvoiceManagerAdminController
 *
 * @author Sooraj
 */
class UnitraiInvoiceManagerAdminController extends ControllerBase {

    public function all() {
        return array(
            '#type' => 'markup',
            '#markup' => t('Displaying List of Invoices in admin section'),
          );
    }
    
    public function unpaid() {
        return array(
            '#type' => 'markup',
            '#markup' => t('Displaying List of UnPaid Invoices in admin section'),
          );
    }
    
    public function paid() {
        return array(
            '#type' => 'markup',
            '#markup' => t('Displaying List of Paid Invoices in admin section'),
          );
    }
    
}
