<?php

/**
@file
Contains \Drupal\unitrai_invoice_manager\Controller\UnitraiInvoiceManagerController.
 */

namespace Drupal\unitrai_invoice_manager\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Description of UnitraiInvoiceManagerController
 *
 * @author Sooraj
 */
class UnitraiInvoiceManagerController extends ControllerBase {

    public function all() {
        return array(
            '#type' => 'markup',
            '#markup' => t('Displaying List of Unpaid Invoices'),
          );
    }
    
    public function unpaid() {
        return array(
            '#type' => 'markup',
            '#markup' => t('Displaying List of UnPaid Invoices'),
          );
    }
    
    public function paid() {
        return array(
            '#type' => 'markup',
            '#markup' => t('Displaying List of Paid Invoices'),
          );
    }
    
}
